#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Stegano - Stegano is a pure Python steganography module.
# Copyright (C) 2010-2019 Cédric Bonhomme - https://www.cedricbonhomme.org
#
# For more information : https://git.sr.ht/~cedric/stegano
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from typing import IO, Union
from base64 import b64decode, b64encode
try:
	import core.utils.tools as tools
except ModuleNotFoundError:
	import tools

def capacity(input_image: Union[str, IO[bytes]], shift:int = 0, intensity: int = 1) -> int:
	img = tools.open_image(input_image)
	width, height = img.size
	capacity = (width * height * 3 * intensity - shift) // 8 
	return capacity - len(str(capacity)) - len(str(capacity)) % 4 - 1

def check(
	input_image: Union[str, IO[bytes]],
	message: str,
	shift: int = 0,
	maxval: int = 0,
	maxshift: int = 0,
	first_run: bool = True,
	maxiter: int = 99
):  
	if first_run:
		message_length = len(message)
		assert message_length != 0, "message length is zero"

		img = tools.open_image(input_image)

		if img.mode not in ["RGB", "RGBA"]:
			img = img.convert("RGB")

		message = str(message_length) + ":" + str(message)
		message_bits = "".join(tools.a2bits_list(message))
		message_bits += "0" * ((3 - (len(message_bits) % 3)) % 3)

	else:
		img = input_image
		message_bits = message
	
	width, height = img.size
	npixels = width * height
	len_message_bits = len(message_bits)
	if len_message_bits > npixels * 3 - shift:
		return maxval, maxshift

	index = 0
	counter = 0
	shift_saved = shift

	for row in range(height):
		for col in range(width):
			if shift != 0:
				shift -= 1
				continue
			if index + 3 <= len_message_bits:

				# Get the colour component.
				pixel = img.getpixel((col, row))
				r = pixel[0]
				g = pixel[1]
				b = pixel[2]

				counter += tools.checklsb(r, message_bits[index])
				counter += tools.checklsb(g, message_bits[index + 1])
				counter += tools.checklsb(b, message_bits[index + 2])

				index += 3

	if maxval < counter:
		maxval = counter
		maxshift = shift_saved

	print(counter, shift_saved, maxval, maxshift)
	if shift_saved < maxiter:
		return check(img, message_bits, shift_saved+1, maxval, maxshift, False, maxiter)
	else:
		return maxval, maxshift

def hide(
	input_image: Union[str, IO[bytes]],
	message: str,
	shift: int = 0,
	intensity: int = 1,
	ignore_least: bool = False
):
	"""Hide a message (string) in an image with the
	LSB (Least Significant Bit) technique.
	"""
	message_length = len(message)
	assert message_length != 0, "message length is zero"
	assert not ignore_least or intensity <= 7, " Intensity is too big, maximum 7 bits without the least"
	assert intensity <= 8, "Pixel is only 8 bit lenght, intensity is too big"

	img = tools.open_image(input_image)

	if img.mode not in ["RGB", "RGBA"]:
		img = img.convert("RGB")

	encoded = img.copy()
	width, height = img.size
	bit_per_pixel = intensity * 3
	index = 0

	if type(message) != bytes: message = message.encode('utf-8')

	message = str(message_length).encode('utf-8') + b":" + message
	message_bits = "".join(tools.a2bits_list(message))
	message_bits += "0" * ((bit_per_pixel - (len(message_bits) % bit_per_pixel)) % bit_per_pixel)

	npixels = width * height
	len_message_bits = len(message_bits)
	if len_message_bits > npixels * bit_per_pixel:
		raise Exception(
			"The message you want to hide is too long: {}".format(message_length)
		)
	for row in range(height):
		for col in range(width):
			if shift != 0:
				shift -= 1
				continue
			if index + bit_per_pixel <= len_message_bits:

				# Get the colour component.
				pixel = img.getpixel((col, row))
				r = pixel[0]
				g = pixel[1]
				b = pixel[2]

				# Change the Least Significant Bit of each colour component.
				if not ignore_least:
					r = tools.setlsb(r, message_bits[index:index + 1 * intensity])
					g = tools.setlsb(g, message_bits[index + 1 * intensity:index + 2 * intensity])
					b = tools.setlsb(b, message_bits[index + 2 * intensity:index + 3 * intensity])
				else:
					r = tools.setnsb(r, message_bits[index:index + 1 * intensity])
					g = tools.setnsb(g, message_bits[index + 1 * intensity:index + 2 * intensity])
					b = tools.setnsb(b, message_bits[index + 2 * intensity:index + 3 * intensity])

				# Save the new pixel
				if img.mode == "RGBA":
					encoded.putpixel((col, row), (r, g, b, pixel[3]))
				else:
					encoded.putpixel((col, row), (r, g, b))

				index += bit_per_pixel
			else:
				img.close()
				return encoded


def reveal(input_image: Union[str, IO[bytes]], shift: int = 0, intensity: int = 1, ignore_least: bool = False):
	"""Find a message in an image (with the LSB technique).
	"""
	assert not ignore_least or intensity <= 7, " Intensity is too big, maximum 7 bits without the least"
	assert intensity <= 8, "Pixel is only 8 bit lenght, intensity is too big"
	
	img = tools.open_image(input_image)
	width, height = img.size
	buff, count = 0, 0
	buff_d = []
	bitab = []
	limit = None
	limlen = 0
	for row in range(height):
		for col in range(width):
			if shift != 0:
				shift -= 1
				continue
			# pixel = [r, g, b] or [r,g,b,a]
			pixel = img.getpixel((col, row))
			if img.mode == "RGBA":
				pixel = pixel[:3]  # ignore the alpha
			for color in pixel:
				if buff_d:
					for elem in buff_d:
						buff += elem << (8 - 1 - count)
						count += 1
					buff_d = []
				for elem in tools.getlsb(color, intensity) if not ignore_least else tools.getnsb(color, intensity):
					if count < 8:
						buff += elem << (8 - 1 - count)
						count += 1
					else:
						buff_d.append(elem)
				#print(buff, count, buff_d)
				if count == 8:
					try:
						bitab.append(tools.getbyte(buff))
					except:
						print(buff, tools.getbyte(buff))
					buff, count = 0, 0
					if bitab[-1] == b":" and limit is None:
						#try:
						limit = int(b"".join(bitab[:-1]))
						limlen = len(b"".join(bitab[:-1]))
						#except Exception:
						#	print("fuck")
			#print(bitab)
			if len(bitab) - limlen - 1 == limit:
				img.close()
				return b"".join(bitab)[limlen + 1:]

	for i in range(len(bitab)):
		if bitab[i] == b':':
			try:
				limit = int(b"".join(bitab[:i]))
				limlen = len(b"".join(bitab[:i]))
				img.close()
				return b"".join(bitab)[limlen + 1:limit+limlen+1]
			except:
				break

	return b""
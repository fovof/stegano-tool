from lzma import LZMACompressor, LZMADecompressor
import brotli

def lzma_compress(text: bytes, force: int = 6) -> bytes:
	c = LZMACompressor(preset=force)
	output = c.compress(text)
	output += c.flush()
	return output

def lzma_decompress(text: bytes) -> bytes:
	c = LZMADecompressor()
	return c.decompress(text)

def brotli_compress(text: bytes):
	return brotli.compress(text)

def brotli_decompress(text: bytes):
	return brotli.decompress(text)
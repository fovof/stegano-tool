import os
import subprocess
import re
import json

from base64 import b64decode, b64encode
from Crypto.Hash import SHA3_512
from Crypto.Cipher import ChaCha20
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad



def hasha(text: bytes) -> bytes:
	return SHA3_512.new(text).digest()

def get_alg_key(algorithm:str) -> bytes:
	algorithms_list = json.loads(open("crypto.json","r").read())

	algorithm_key = b""

	for index, algname in algorithms_list.items():
		if algname == algorithm:
			return int(index).to_bytes(1, "big")

	raise ValueError("Internal Error: Algorithm not found")

def get_alg_by_key(key:bytes) -> str:
	algorithms_list = json.loads(open("crypto.json","r").read())

	try:
		return algorithms_list[str(key)]
	except:
		raise ValueError("Internal Error: Algorithm not found")


def aes_crypt(text: bytes, password: bytes, mode: str) -> bytes:

	try:
	#Пароль дополняется до 32 байт
		if len(password) < 32:
			password += hasha(password)[0:32-len(password)]

		#Проверяется длина пароля
		if len(password) == 32:

			#Инициализируется шифратор с заданным паролем и модом
			cipher = AES.new(password, AES.MODE_ECB)

			#в зависимости от mode функции запускается шифрование или дешифровка
			if mode == 'encrypt':
				ciphertext = cipher.encrypt(pad(text,32)) #pad используется для выравнивания входных данных по длине блока
				return ciphertext

			elif mode == 'decrypt':
				plaintext = cipher.decrypt(text)
				return unpad(plaintext,32) #unpad делает действие обратное pad

			else:
				print('Wrong mode. Use \'encrypt\' or \'decrypt\'.')

		else:
			print('Too many symbols!')

	except ValueError:
		print("\n{0} failed.\n".format(mode))
		raise Exception("Error during (de/en)cryption")

def chacha_crypt(text: bytes, password: bytes, mode: str) -> bytes:

	try:
		#Пароль дополняется до 32 байт
		if len(password) < 32:
			password += hasha(password)[0:32-len(password)]

		#Проверяется длина пароля
		if len(password) == 32:

			#в зависимости от mode функции запускается шифрование или дешифровка
			if mode == 'encrypt':
				#Инициализируется шифратор с заданным паролем
				cipher = ChaCha20.new(key=password)
				ciphertext = cipher.encrypt(text) 

				return cipher.nonce + ciphertext

			elif mode == 'decrypt':
				nonce = text[0:12]
				ciphertext = text[12:len(text)]

				#Инициализируется шифратор с заданным паролем и number used once
				cipher = ChaCha20.new(key=password, nonce=nonce)

				plaintext = cipher.decrypt(ciphertext)
				return plaintext 

			else:
				print('Wrong mode. Use \'encrypt\' or \'decrypt\'.')

		else:
			print('Too many symbols!')

	except:

		return b""


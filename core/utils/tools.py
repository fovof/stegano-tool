#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Stegano - Stegano is a pure Python steganography module.
# Copyright (C) 2010-2019 Cédric Bonhomme - https://www.cedricbonhomme.org
#
# For more information : https://git.sr.ht/~cedric/stegano
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import base64
import itertools
from functools import reduce
from typing import IO, Iterator, List, Tuple, Union

from PIL import Image

#+
def a2bits_list(chars) -> List[str]:
	"""Convert a string to its bits representation as a list of 0's and 1's.

	>>>  a2bits_list("Hello World!")
	['01001000',
	'01100101',
	'01101100',
	'01101100',
	'01101111',
	'00100000',
	'01010111',
	'01101111',
	'01110010',
	'01101100',
	'01100100',
	'00100001']
	>>> "".join(a2bits_list("Hello World!"))
	'010010000110010101101100011011000110111100100000010101110110111101110010011011000110010000100001'
	"""
	if isinstance(chars, str):
		return [bin(ord(x))[2:].rjust(8, "0") for x in chars]
	elif isinstance(chars, bytes):
		return [bin(x)[2:].rjust(8, "0") for x in chars]
#+

def getbyte(num):
	h = hex(num)[2:]
	if len(h) == 1:
		h = '0' + h

	return bytes.fromhex(h)

def setlsb(component: int, bit: str) -> int:
	"""Set Least Significant Bit of a colour component.
	"""
	return component & ~int('1'*len(bit),2) | int(bit,2)

def getlsb(component: int, intensity: int) -> int:
	comp = "{0:b}".format(component & int("1"*intensity,2))
	
	return [int(x) for x in comp.rjust(intensity,'0')]

def checklsb(component: int, bit: str) -> bool:
	return (component & 1) == int(bit)

def setnsb(component: int, bit: str) -> int:
	return component & ~int('1'*len(bit)+"0",2) | int(bit+"0",2)

def getnsb(component: int, intensity: int) -> int:
	comp = "{0:b}".format(component & int("1"*intensity+"0",2))[:-1]

	return [int(x) for x in comp.rjust(intensity,'0')]

#+
def open_image(fname_or_instance: Union[str, IO[bytes]]):
	"""Opens a Image and returns it.

	:param fname_or_instance: Can either be the location of the image as a
							  string or the Image.Image instance itself.
	"""
#	if isinstance(fname_or_instance, bytes):
#		return Image.frombytes()

	if isinstance(fname_or_instance, Image.Image):
		return fname_or_instance

	return Image.open(fname_or_instance)

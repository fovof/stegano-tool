#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Stegano - Stegano is a pure Python steganography module.
# Copyright (C) 2010-2019 Cédric Bonhomme - https://www.cedricbonhomme.org
#
# For more information : https://git.sr.ht/~cedric/stegano
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

__author__ = "Cedric Bonhomme"
__version__ = "$Revision: 0.3 $"
__date__ = "$Date: 2011/12/28 $"
__revision__ = "$Date: 2019/06/04 $"
__license__ = "GPLv3"

import itertools
import math
from typing import Dict, Iterator, List



polys = {
    2: [2, 1],
    3: [3, 1],
    4: [4, 1],
    5: [5, 2],
    6: [6, 1],
    7: [7, 1],
    8: [8, 4, 3, 2],
    9: [9, 4],
    10: [10, 3],
    11: [11, 2],
    12: [12, 6, 4, 1],
    13: [13, 4, 3, 1],
    14: [14, 8, 6, 1],
    15: [15, 1],
    16: [16, 12, 3, 1],
    17: [17, 3],
    18: [18, 7],
    19: [19, 5, 2, 1],
    20: [20, 3],
    21: [21, 2],
    22: [22, 1],
    23: [23, 5],
    24: [24, 7, 2, 1],
    25: [25, 3],
    26: [26, 6, 2, 1],
    27: [27, 5, 2, 1],
    28: [28, 3],
    29: [29, 2],
    30: [30, 23, 2, 1],
    31: [31, 3],
}


def LFSR(m: int) -> Iterator[int]:
    """LFSR generator of the given size
    https://en.wikipedia.org/wiki/Linear-feedback_shift_register
    """
    n: int = m.bit_length()
    # Set initial state to {1 0 0 ... 1}
    state: List[int] = [0] * n
    state[0] = 1
    state[-1] = 1
    feedback: int = 0
    poly: List[int] = polys[n]
    while True:
        # Compute the feedback bit
        feedback = 0
        for i in range(len(poly)):
            feedback = feedback ^ state[poly[i] - 1]
        # Roll the registers
        state.pop()
        # Add the feedback bit
        state.insert(0, feedback)
        # Convert the registers to an int
        out = sum([e * (2 ** i) for i, e in enumerate(state)])
        if out < m: yield out
import unireedsolomon as rs
from math import ceil, floor

def rs_xencode(inp: bytes):
	assert len(inp) < 127, "Too big for one piece"
	rs_coder = rs.RSCoder(len(inp)*2,len(inp))

	return bytes(rs_coder.encode(inp, return_string=False))

def rs_xdecode(inp: bytes):
	assert len(inp) < 255, "Too big for one piece"
	rs_coder = rs.RSCoder(len(inp),len(inp)//2)

	return bytes(rs_coder.decode(inp, return_string=False)[0])

def rs_encode(inp: bytes, redundancy: int):
	input_length = len(inp)
	redundancy_length = ceil(255 / 100 * redundancy)
	cleartext_length = 255 - redundancy_length

	rs_coder = rs.RSCoder(255,cleartext_length)
	output = b""

	for i in range(0,input_length//cleartext_length+1):
		if i == input_length//cleartext_length:
			part = inp[i*cleartext_length:input_length].ljust(cleartext_length,b"U")
		else:
			part = inp[i*cleartext_length:cleartext_length*(i+1)]

		encoded = rs_coder.encode(part, return_string=False)
		output += bytes(encoded)

	return output

def rs_decode(inp: bytes, redundancy: int):
	input_length = len(inp)
	redundancy_length = ceil(255 / 100 * redundancy)
	cleartext_length = 255 - redundancy_length

	rs_coder = rs.RSCoder(255,cleartext_length)
	output = b""

	for i in range(0,input_length//255):
		part = inp[i*255:255*(i+1)]
		encoded = rs_coder.decode(part, return_string=False, nostrip=True)
		output += bytes(encoded[0])

	return output.rstrip(b"U")

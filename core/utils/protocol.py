import re, json
from core.utils.encryptor import hasha

spacer_one = b"spacer"

def parse_file(filebytes: bytes, hidden: bool = False, password: bytes = b"") -> (str, bytes):

	algorithms_list = json.loads(open("crypto.json","r").read())

	if hidden:
		#XOR spacer one with first 6 bytes of tex
		print(type(filebytes))
		spacer = (int.from_bytes(filebytes[1:7], "big") ^ int.from_bytes(filebytes[0:6], "big")).to_bytes(6, "big")
	else:
		spacer = spacer_one

	try:
		stegotext = re.split(spacer, filebytes)[1]
	except IndexError:
		raise Exception("Container can't be found")


	filehash = stegotext[0:8]
	
	hidetext = stegotext[8:len(stegotext)]

	hidehash = hasha(hidetext+password)[8:16]

	if hidehash == filehash:
		return hidetext
	else:
		raise Exception("Authencity can't be verified")


def create_file(filebytes: bytes, hidebytes: bytes, hidden: bool = False, password: bytes = b"") -> bytes:


	if hidden:
		
		spacer = (int.from_bytes(filebytes[1:7], "big") ^ int.from_bytes(filebytes[0:6], "big")).to_bytes(6, "big")
	else:
		spacer = spacer_one

	hidehash = hasha(hidebytes+password)[8:16]

	return b''.join([filebytes, spacer, hidehash, hidebytes])

def parse_container(text: bytes) -> (dict):
	
	output = {}

	text_arr = re.split(rb":!:", text)

	for filetext in text_arr:
		file_arr = re.split(rb":\.:", filetext)
		output[file_arr[0]] = file_arr[1]


	return output

def ceil_div(num: int, div: int) -> int:

	q, m = divmod(num, div)
	if m: q += 1
	return q

def byte_size(num: int) -> int:

	if num == 0:
		return 1
	return ceil_div(num.bit_length(), 8)

def create_container(dict_in: dict) -> bytes:

	output = b""

	for key in dict_in.keys():
		output += b":.:".join([key, dict_in[key]]) + b":!:"


	output = output[0:-3]

	return output

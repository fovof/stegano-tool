import core.utils.protocol as protocol
from io import BytesIO
from core.utils.encryptor import *
from core.utils.compressor import *
from core.utils.fixer import *
import core.utils.lsb as lsb
import core.utils.lsb_lfsr as lsb_lfsr
import json

modes_list = json.loads(open("formats.json","r").read())

def packing(dict_in: dict, encryption: str, password: bytes, compression: str, redundancy: int):

	assert redundancy < 95, "redundancy must be less than 95"

	bytes_to_hide = protocol.create_container(dict_in)

	print("Size after packing {0} bytes".format(len(bytes_to_hide)))

	if compression == 'lzma':
		compressed = lzma_compress(bytes_to_hide)
		comp_key = b"l"
		print('Compressed by lzma.')
	elif compression == 'brotli':
		compressed = brotli_compress(bytes_to_hide)
		comp_key = b"2"
		print('Compressed by brotli.')
	elif compression == 'best':

		compressed_lzma = lzma_compress(bytes_to_hide)
		compressed_brotli = brotli_compress(bytes_to_hide)
		if len(compressed_brotli) < len(compressed_lzma):
			compressed = compressed_brotli
			comp_key = b"2"
			print('Compressed by brotli.')
		else:
			compressed = compressed_lzma
			comp_key = b"l"
			print('Compressed by lzma.')

	else:
		compressed = bytes_to_hide
		comp_key = b"0"

	print("Size after compression {0} bytes".format(len(compressed)))

	if len(compressed) <= len(bytes_to_hide):
		bytes_to_hide = compressed
	else:
		print("Compression irrational, so won't be used")
		comp_key = b"0"


	if encryption == 'AES_256':
		bytes_to_hide = aes_crypt(bytes_to_hide, password, 'encrypt')
	elif encryption == 'ChaCha20':
		bytes_to_hide = chacha_crypt(bytes_to_hide, password, 'encrypt')

	if redundancy > 0: bytes_to_hide = rs_encode(bytes_to_hide, redundancy)

	bytes_to_hide += rs_xencode(str(redundancy).rjust(2,'0').encode('utf-8') + get_alg_key(encryption) + comp_key)

	return bytes_to_hide

def unpacking(packed_bytes: bytes, password: bytes):
	rec = rs_xdecode(packed_bytes[-8:])
	redundancy = int(rec[-4:-2])
	encryption = get_alg_by_key(rec[-2])
	comp_key = rec[-1]

	if redundancy > 0: 
		packed_bytes = rs_decode(packed_bytes[:-8],redundancy)
	else:
		packed_bytes = packed_bytes[:-8]

	print("Redundancy: {0}%; Encryption: {1}".format(redundancy,encryption))

	if encryption == 'AES_256':
		packed_bytes = aes_crypt(packed_bytes, password, 'decrypt')
	elif encryption == 'ChaCha20':
		packed_bytes = chacha_crypt(packed_bytes, password, 'decrypt')
	elif encryption == 'none':
		packed_bytes = packed_bytes


	if chr(comp_key) == 'l':
		packed_bytes = lzma_decompress(packed_bytes)
		print("Compression: lzma")
	elif chr(comp_key) == '2':
		packed_bytes = brotli_decompress(packed_bytes)
		print("Compression: brotli")

	output = protocol.parse_container(packed_bytes)

	print("Extraction complete.")

	return output


def addition_pack(dict_in: dict, dict_cont: dict, encryption: str, password: str, compression: str, redundancy: int) -> dict:

	password = password.encode('utf-8')
	
	bytes_to_hide = packing(dict_in, encryption, password, compression, redundancy)

	cont_count = len(dict_cont.keys())

	bth_count = len(bytes_to_hide)

	bytes_per_cont = bth_count // cont_count

	splitter = bytes_per_cont

	output = {}

	for filename in dict_cont.keys():
		if not dict_cont[filename][1].lower() in modes_list['addition']['formats'].split(','):
			print(dict_cont[filename][1], ' is not supported format for addition.')
			del dict_cont[filename]
		else:
			if splitter + bytes_per_cont > bth_count:
				hide = bytes_to_hide[splitter-bytes_per_cont:bth_count]
			else:
				hide = bytes_to_hide[splitter-bytes_per_cont:splitter]

			print(len(hide))
			outputfilebytes = protocol.create_file(dict_cont[filename][0], hide, hidden=True, password=password)

			output[filename] = outputfilebytes

			splitter += bytes_per_cont

	assert len(dict_cont) > 0, "No supported containers found"

	return output

def addition_unpack(dict_in: dict, password: str) -> dict:
	password = password.encode('utf-8')

	for key in dict_in.keys():
		if not dict_in[key][1].lower() in modes_list['addition']['formats'].split(','):
			print(dict_cont[key][1], ' is not supported format for addition.')
			del dict_in[key]

	assert len(dict_in) > 0, "No supported files found"

	encrypted_bytes = b""

	for filename in dict_in.keys():
		hidetext = protocol.parse_file(dict_in[filename][0], hidden=True, password=password)

		encrypted_bytes += hidetext

	return unpacking(encrypted_bytes, password)


def lsb_pack(dict_in: dict, dict_cont: dict, encryption: str, password: str, shift: int, intensity: int, compression: str, redundancy, secure: bool = False, ignore_least: bool = False) -> dict:

	password = password.encode('utf-8')

	bytes_to_hide = packing(dict_in, encryption, password, compression, redundancy)

	cont_count = len(dict_cont.keys())

	bth_count = len(bytes_to_hide)

	dict_cont_capacity = 0

	for key in dict_cont.keys():
		if not dict_cont[key][1].lower() in modes_list['lsb']['formats'].split(','):
			print(dict_cont[key][1], ' is not supported format for lsb.')
			del dict_cont[key]
		else:
			dict_cont[key][0] = BytesIO(dict_cont[key][0])
			dict_cont_capacity += lsb.capacity(dict_cont[key][0], shift, intensity)

	assert len(dict_cont) > 0, "No supported containers found"
	print(bth_count, dict_cont_capacity)
	assert bth_count < dict_cont_capacity, "Container capacity is too small"

	bytes_per_cont = bth_count // cont_count

	splitter = bytes_per_cont

	output = {}

	for filename in dict_cont.keys():
		
		if splitter + bytes_per_cont > bth_count:
			hide = bytes_to_hide[splitter-bytes_per_cont:bth_count]
		else:
			hide = bytes_to_hide[splitter-bytes_per_cont:splitter]

		hide_res = lsb.hide(dict_cont[filename][0], hide, shift, intensity, ignore_least) if not secure else lsb_lfsr.hide(dict_cont[filename][0], hide, shift, intensity, ignore_least)

		output[filename] = img_to_bytes(hide_res, dict_cont[filename][1])

		splitter += bytes_per_cont

	return output

def lsb_unpack(dict_in: dict, password: str, shift: int = 0, intensity: int = 1, secure: bool = False, ignore_least: bool = False) -> dict:
	password = password.encode('utf-8')

	for key in dict_in.keys():
		if not dict_in[key][1].lower() in modes_list['lsb']['formats'].split(','):
			print(dict_cont[key][1], ' is not supported format for lsb.')
			del dict_in[key]

	assert len(dict_in) > 0, "No supported files found"

	encrypted_bytes = b""

	for filename in dict_in.keys():
		encrypted_bytes += lsb.reveal(BytesIO(dict_in[filename][0]), shift, intensity, ignore_least) if not secure else lsb_lfsr.reveal(BytesIO(dict_in[filename][0]), shift, intensity, ignore_least)

	return unpacking(encrypted_bytes, password)

def lsb_check(dict_in: dict, dict_cont: dict, encryption: str, password: str, compression: str, redundancy: int):
	password = password.encode('utf-8')

	bytes_to_hide = packing(dict_in, encryption, password, compression, redundancy)

	bth_count = len(bytes_to_hide)

	max_corr = 0
	max_shift = 0
	max_key = ''
	for key in dict_cont.keys():
		if bth_count >= lsb.capacity(dict_cont[key][0]) or not dict_cont[key][1] in modes_list['lsb']['formats']:
			del dict_cont[key]
		else:
			corr, shift = lsb.check(dict_cont[key][0], bytes_to_hide)
			if corr > max_corr:
				max_corr = corr
				max_shift = shift
				max_key = key

	return max_key, max_shift
	
def img_to_bytes(img, fmt):
	output = BytesIO()
	img.save(output, format=fmt)
	return output.getvalue()

def mixed_pack(dict_in: dict, dict_cont: dict, password: str, encryption: str) -> dict:
	pass
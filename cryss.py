#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import argparse
import json
import core.stego as stego

modes_list = json.loads(open("formats.json","r").read())
modes = [x for x in modes_list.keys()]
encryption_list = json.loads(open("crypto.json","r").read())
encryptions = [encryption_list[key] for key in encryption_list.keys()]


#ничего особенного, просто парcер командной строки, смотри дальше
def createParser():
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-?', '--help', action='store_const', const=True, default=False)
	parser.add_argument('-h', '--hide', action='store_const', const=True, default=False)
	parser.add_argument('-r', '--reveal', action='store_const', const=True, default=False)
	parser.add_argument('-cC', '--check', action='store_const', const=True, default=False)
	parser.add_argument('-I', '--ignoreleast', action='store_const', const=True, default=False)
	parser.add_argument('-p', '--password', default=None, type=str)
	parser.add_argument('-m', '--mode', default='addition', type=str)
	parser.add_argument('-e', '--encryption', default='none', type=str)
	parser.add_argument('-C', '--compression', default='best', type=str)
	parser.add_argument('-i', '--input', default=None, type=str)
	parser.add_argument('-c', '--container', default=None, type=str)
	parser.add_argument('-o', '--output', default=None, type=str)
	parser.add_argument('-s', '--shift', default=0, type=int)
	parser.add_argument('-l', '--lsbdepth', default=1, type=int)
	parser.add_argument('-R', '--redundancy', default=0, type=int)
	return parser

def help():
	print('\n\t  \\  Cryss v0.1  /')
	print('\t   \\------------/\n')
	print('\tCRYPTO STEGANOGRAPHY')
	description = '|  tool for secure steganography  |'
	print('|'+'-'*(len(description)-2)+'|')
	print(description)
	print('|'+'-'*(len(description)-2)+'|\n')
	print('USAGE: cryss -h/-r -p [PASSWORD] -i [INPUT] -c [CONTAINER] -o [OUTPUT] -m [MODE] -e [ENCRYPTION] -s [LSB_SHIFT] -l [LSB_DEPTH]\n')
	print(' -p --password \t: password for container')
	print(' -i --input \t\t: input file(s) or directory to put into container or to reveal information')
	print(' -c --container \t: container to put files in')
	print(' -o --output \t\t: output folder')
	print(' -m --mode \t\t: can be \'addition\' (default), \'lsb\' or \'secure_lsb\'')
	print(' -e --encryption \t: specify encryption algorythm \'none\' (default), \'AES_256\' or \'ChaCha20\' (stream cypher)')
	print(' -C --compression \t: specify compression algorythm \'best\' (default), \'lzma\', \'brotli\' or \'none\'')
	print(' -s --shift \t\t: initial shift for lsb method (default 0)')
	print(' -l --lsbdepth \t: specifies how many bits will be rewritten by lsb method (default 1)')
	print(' -R --redundancy \t: specifies redundancy in percentage of input volume (default 0)')
	print(' -? --help \t\t: prints out this page')

	print('\n\tEXAMPLE: \n')


def get_files(dirr):
	if os.access(dirr, os.R_OK):  #TODO Recursive getting files from all folders
		try:
			dir_files = os.listdir(dirr)
			for i in range(0,len(dir_files)):
				dir_files[i] = dirr +"/"+dir_files[i]
			return dir_files
		except:
			return dirr.split(',')
	else:
		raise Exception("No directory/file or no access")

def hide(inp: str, container: str, output: str, mode: str, shift: int, intensity: int, encryption: str, password: str, compression: str, redundancy: int, ignore_least: bool):
	input_files = get_files(inp)
	container_files = get_files(container)

	input_files_dict = {}
	for file in input_files:
		filename = "".join(file.split("/")[1:])
		input_files_dict[filename.encode("utf-8")] = open(file, 'rb').read()

	container_files_dict = {}
	for file in container_files:
		filename = "".join(file.split("/")[1:])
		container_files_dict[filename.encode('utf-8')] = [open(file, 'rb').read(), file.split(".")[-1]]

	if mode == "addition":
		result = stego.addition_pack(input_files_dict, container_files_dict, encryption, password, compression, redundancy)
	elif mode == "lsb":
		result = stego.lsb_pack(input_files_dict, container_files_dict, encryption, password, shift, intensity, compression, redundancy, ignore_least=ignore_least)
	elif mode == "secure_lsb":
		result = stego.lsb_pack(input_files_dict, container_files_dict, encryption, password, shift, intensity, compression, redundancy, secure=True, ignore_least=ignore_least)
	elif mode == "mixed":
		result = stego.mixed_pack(input_files_dict, container_files_dict, encryption, password, compression, redundancy)

	for file in result.keys():
		with open(output + "/" + file.decode('utf-8'), 'wb') as f:
			f.write(result[file])
			f.close()


def reveal(inp: str, output:str, mode: str, shift: int, intensity: int, password: str, ignore_least: bool):
	input_files = get_files(inp)

	input_files_dict = {}
	for file in input_files:
		input_files_dict[file.encode("utf-8")] = [open(file, 'rb').read(), file.split(".")[-1]]

	if mode == "addition":
		result = stego.addition_unpack(input_files_dict, password)
	elif mode == "lsb":
		result = stego.lsb_unpack(input_files_dict, password, shift, intensity, ignore_least=ignore_least)
	elif mode == "secure_lsb":
		result = stego.lsb_unpack(input_files_dict, password, shift, intensity, secure=True, ignore_least=ignore_least)
	elif mode == "mixed":
		result = stego.mixed_unpack(input_files_dict, password)

	for filename in result.keys():
		with open(output+"/"+filename.decode('utf-8'), 'wb') as f:
			f.write(result[filename])
			f.close()

def check(inp: str, container: str, mode: str, shift: int, intensity: int, encryption: str, password: str, compression: str):
	pass

#основное тело скрипта
if __name__=="__main__":
	
	#объявляем парсер, забираем ключи
	parser = createParser()
	key = parser.parse_args()

	if key.help:
		help()
		sys.exit(1)

	if (not (key.hide ^ key.reveal)) or not (key.input and key.output):
		print("All required options must be specified")
		sys.exit(1)

	if not key.mode in modes:
		print("Mode is unrecognisable")
		sys.exit(1)

	if not key.encryption in encryptions:
		print("Encryption is unrecognisable")
		sys.exit(1)

	if key.check:
		check(key.input, key.container, key.mode, key.shift, key.lsbdepth, key.encryption, key.password, key.compression, key.redundancy)

	if key.hide:
		hide(key.input, key.container, key.output, key.mode, key.shift, key.lsbdepth, key.encryption, key.password, key.compression, key.redundancy, key.ignoreleast)

	elif key.reveal:
		reveal(key.input, key.output, key.mode, key.shift, key.lsbdepth, key.password, key.ignoreleast)

from tkinter import filedialog as fd
#file_name = fd.askopenfilename()
import gui
import sys
try:
	from PyQt5 import QtWidgets
except:
	import os
	os.system("py -m pip install pyqt5")
	os.system("pip3 install pyqt5")

from subprocess import Popen, PIPE


def list_to_str(arr):
	if type(arr) == str:
		return arr
	print(arr)
	text = ""
	for elem in arr:
		text += elem + ","
	return text[:-1]

class Controller():
	def __init__(self):
		self.ifnames = "M:\\stegano\\project\\inp"
		self.cfnames = "M:\\stegano\\project\\cont"
		self.odname = 'M:\\stegano\\project\\output'

		self.cyphermethod = 'none'
		self.cypherpassword = None
		self.redundancy = 0

		self.stegomethod = 'secure_lsb'
		self.ignore_least = False
		self.intensity = 1
		self.shift = 0

		self.compressmethod = 'none'

	def setif(self, fnames):
		self.ifnames = list_to_str(fnames)

	def setcf(self, fnames):
		self.cfnames = list_to_str(fnames)

	def setod(self, dname):
		self.odname = dname

	def setcrypto(self, text):
		self.cyphermethod = text

	def setstego(self, text):
		self.stegomethod = text

	def setcompress(self, text):
		self.compressmethod = text

	def setintensity(self, num):
		self.intensity = num

	def setshift(self, num):
		self.shift = num

	def setredundancy(self, num):
		self.redundancy = num 

	def setpass(self, text):
		self.cypherpassword = text

	def setignoreleast(self, val):
		self.ignore_least = True if val else False

	def pack(self):
		cmd = ["cryss","-h","-i",str(self.ifnames),"-c",str(self.cfnames),
		"-o",self.odname,"-m",self.stegomethod,"-e",self.cyphermethod,
		"-p",str(self.cypherpassword),"-C",self.compressmethod,"-s",str(self.shift),
		"-l",str(self.intensity),"-R",str(self.redundancy)]
		if self.ignore_least: cmd.append("-I")
		with Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True) as proc:
			output = proc.stdout.read()
			error = proc.stderr.read()
			try:
				return output.decode('utf-8') + error.decode('utf-8')
			except UnicodeDecodeError:
				return output.decode('cp866') + error.decode('cp866')

	def unpack(self):
		cmd = ["cryss","-r","-i",str(self.ifnames),"-o",self.odname,
		"-m",self.stegomethod,"-e",self.cyphermethod,"-p",str(self.cypherpassword),
		"-s",str(self.shift),"-l",str(self.intensity)]
		if self.ignore_least: cmd.append("-I")
		with Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True) as proc:
			output = proc.stdout.read()
			error = proc.stderr.read()
			try:
				return output.decode('utf-8') + error.decode('utf-8')
			except UnicodeDecodeError:
				return output.decode('cp866') + error.decode('cp866')


class CryssApp(QtWidgets.QMainWindow, gui.Ui_MainWindow):
	def __init__(self):
		super().__init__()
		self.setupUi(self)

		self.buttonOpenInput.clicked.connect(self.showInputDialog)
		self.buttonOpenContainer.clicked.connect(self.showContainerDialog)
		self.buttonOpenOutput.clicked.connect(self.showOutputDialog)

		self.lineInput.editingFinished.connect(self.setIF)
		self.lineContainer.editingFinished.connect(self.setCF)
		self.lineOutput.editingFinished.connect(self.setOD)
		self.lineEditKey.editingFinished.connect(self.setPass)

		self.comboBoxCrypto.currentTextChanged.connect(self.setCrypto)
		self.comboBoxSteganoMethod.currentTextChanged.connect(self.setStego)
		self.comboBoxCompression.currentTextChanged.connect(self.setCompress)

		self.spinBoxIntense.valueChanged.connect(self.setIntensity)
		self.spinBoxRedundancy.valueChanged.connect(self.setRedundancy)
		self.spinBoxShift.valueChanged.connect(self.setShift)

		self.buttonPack.clicked.connect(self.Pack)
		self.buttonUnpack.clicked.connect(self.Unpack)
		self.checkBoxIgnoreLeast.stateChanged.connect(self.setIgnoreLeast)

		self.ctrl = Controller()

		#Отладка
		self.lineInput.setText(self.ctrl.ifnames)
		self.lineContainer.setText(self.ctrl.cfnames)
		self.lineOutput.setText(self.ctrl.odname)

	def showInputDialog(self):
		res = QtWidgets.QFileDialog.getOpenFileNames(self, 'Select file(s)')
		self.ctrl.setif(res[0])
		self.lineInput.setText(self.ctrl.ifnames)

	def showContainerDialog(self):
		res = QtWidgets.QFileDialog.getOpenFileNames(self, 'Select file(s)')
		self.ctrl.setcf(res[0])
		self.lineContainer.setText(self.ctrl.cfnames)

	def showOutputDialog(self):
		res = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select directory')
		self.ctrl.setod(res)
		self.lineOutput.setText(self.ctrl.odname)

	def setIF(self):
		self.ctrl.setif(self.lineInput.text())

	def setCF(self):
		self.ctrl.setcf(self.lineContainer.text())

	def setOD(self):
		self.ctrl.setod(self.lineOutput.text())

	def setPass(self):
		self.ctrl.setpass(self.lineEditKey.text())

	def setCrypto(self, t):
		self.ctrl.setcrypto(t)
		#self.ctrl.setcypher(self.comboBoxCrypto.currentText())

	def setStego(self, t):
		self.ctrl.setstego(t)
		#self.ctrl.setstego(self.comboSteganoMethod.currentText())

	def setCompress(self, t):
		self.ctrl.setcompress(t)
		#self.ctrl.setcompress(self.comboBoxCompression.currentText())

	def setIntensity(self, n):
		self.ctrl.setintensity(n)
		#self.ctrl.setcypher(self.comboBoxCrypto.currentText())

	def setRedundancy(self, n):
		self.ctrl.setredundancy(n)
		#self.ctrl.setstego(self.comboSteganoMethod.currentText())

	def setShift(self, n):
		self.ctrl.setshift(n)
		#self.ctrl.setcompress(self.comboBoxCompression.currentText())

	def setIgnoreLeast(self, val):
		self.ctrl.setignoreleast(val)

	def Pack(self):
		self.textConsole.clear()
		self.textConsole.insertPlainText(self.ctrl.pack())

	def Unpack(self):
		self.textConsole.clear()
		self.textConsole.insertPlainText(self.ctrl.unpack())


def main():
	app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
	window = CryssApp()  # Создаём объект класса CryssApp
	window.show()  # Показываем окно
	app.exec_()  # и запускаем приложение


if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
	main()